import axios from 'axios'

export default {
    getPhotos() {
        return axios.get('https://cors-anywhere.herokuapp.com/http://gallery.dev.webant.ru/api/photos').then(response => response.data)
    }
}