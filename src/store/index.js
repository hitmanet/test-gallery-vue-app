import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store ({
    state: {
        photos: {},
        paginatedData: []
    },
    mutations: {
        savePhotos(state, photos) {
            state.photos = {...state.photos, ...photos }
        },
        savePaginatedData(state, data){
            state.paginatedData = data
        }
    }
})