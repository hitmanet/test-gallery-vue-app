import Vue from 'vue'
import Router from 'vue-router'
import App from '../App'
import Gallery from '../components/Gallery.vue'
import NoConnection from '../components/NoConnection.vue'

Vue.use(Router)

export default new Router({
    routes: [
        {
            path: '/',
            name: 'new',
            component: Gallery,
        },
        {
            path: '/popular',
            name: 'popular',
            component: NoConnection
        }
    ]
})